#!/usr/bin/env bash

# Usage direnvcopy seriesname filenamepattern

USAGE="Usage: $0 INFOLDER OUTFOLDER ['\${VAR1} \${VAR2} ...']"
USAGE2="       Recursively copies INFOLDER to OUTFOLDER. If a file with"
USAGE3="       extension .template is found, its ENVVARS will be resolved."
USAGE4="       You can specify specific envvars to substitute with the last"
USAGE5="       parameter. In this case, any other will be left untouched."

COPYRIGHT="direnvcopy - (C) 2020 Nebular Streams"

export INFOLDER=$1
export OUTFOLDER=$2

SOFTWARE=$(which envsubst)

if [[ -z ${SOFTWARE} ]]; then
  echo "! This application requires GNU gettext-base package installed, and specifically the tool envsubst"
  echo "- OSX   : brew install gettext"
  echo "- Linux : apt install gettext-base"
  exit 3
fi
if [[ -z "${OUTFOLDER}" ]]; then
  echo "${COPYRIGHT}"
  echo "${USAGE}"
  echo "${USAGE2}"
  echo "${USAGE3}"
  echo "${USAGE4}"
  echo "${USAGE5}"
  exit 2
else
  export OUTFOLDER=$(realpath "${OUTFOLDER}")
fi
if [[ -z "${INFOLDER}" ]]; then
  echo "${COPYRIGHT}"
  echo "${USAGE}"
  echo "${USAGE2}"
  echo "${USAGE3}"
  echo "${USAGE4}"
  echo "${USAGE5}"
  exit 1
fi

if [[ ! -d "${INFOLDER}" ]]; then
  echo "! Source folder ${INFOLDER} does not exist."
  exit 4
fi

export TEMPLATE=$3

cd "${INFOLDER}"

find . -exec bash -c '
  for file do
    f=$(echo $file | cut -b 3-)
    if [[ -f "${f}" ]]; then

      filedir=$(dirname "${f}")

      if [[ ! -d ${OUTFOLDER}/$filedir ]]; then
        mkdir -p "${OUTFOLDER}/${filedir}"
        echo "- creating ${OUTFOLDER}/${filedir}"
      fi

      filename=$(basename -- "${f}")
      extension="${filename##*.}"
      filename="${filename%.*}"

      if [[ "${extension}" == "template" ]]; then
        # we have a template to envsubst
        echo "- Template for ${f} into ${OUTFOLDER}/${filedir}/${filename}"
        envsubst ${TEMPLATE} <"${f}" >"${OUTFOLDER}/${filedir}/${filename}"
      else
        # A regular File
        echo "+ ${f}"
        cp "${f}" "${OUTFOLDER}/${f}"
      fi
    fi

  done' find-sh {} +
