# direnvcopy #

A utility to copy a folder to another folder, substituting ENVVARS inside files with
```.template``` extensions. The ```.template``` extension is then removed in the final file.
Files without the ```.template``` extension will just be copied without any parsing.

### Installation ###

Just install the tool on your computer:

```npm install -g direnvcopy```

### Usage ###

Just run the program with your desired parameters:

```direnvcopy infolder destination [templatevars]```

* **infolder** Source folder
* **destination** Destination Folder
* **templatevars** Substitute only these vars (all if not provided)

### Example
```direnvcopy /home/john/folder /dist/folder```
- Copies from /home/john/folder to /dist/folder, substituting ALL envvars found  on files that end with .template

```direnvcopy /home/john/folder /dist/folder '${INSTALL_PATH}'```
- Copies from /home/john/folder to /dist/folder, substituting only the envvar ```${INSTALL_PATH}``` on files that end with ``.template```. The rest will be left untouched.

### Contribution guidelines ###

Contributions welcome.

### License

Creative Commons CC-BY-SA 3.0
